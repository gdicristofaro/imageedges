#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QtWidgets>


using std::cout;
using std::logic_error;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}



union pixel {
  uint ARGB;
  struct {
    unsigned char b;
    unsigned char g;
    unsigned char r;
    unsigned char a;
  };

  operator uint () const { return ARGB; }
};


// convolution will be applied to each pixel where mask is matrix and then pixel is multiplied multiplier
class ConvolutionMask {
  int w;
  int h;

  int halfW;
  int halfH;

  double multip;
  const short* mask;  // [x + w * y]

  public:
    ConvolutionMask(const short* convMask, int width, int height, double multiplier) :
        w(width), h(height), multip(multiplier), mask(convMask) {

        if (width % 2 != 1 || height % 2 != 1)
            throw logic_error("mask width and height must be odd");

        halfW = w / 2;
        halfH = h / 2;
    }

    short getMask(int x, int y) const {
        return mask[x + w * y];
    }

    pixel getGreyScale(const QImage& orig, int x, int y) const {
        int totItems = 0;
        long val = 0;

        for (int convX = 0; convX < w; convX++) {
            int pixelX = x + convX - halfW;
            // skip this convolution if pixel X outside of img range
            if (pixelX < 0 || pixelX >= orig.width())
                continue;

            for (int convY = 0; convY < h; convY++) {
                int pixelY = y + convY - halfH;
                // skip this convolution if pixel Y outside of img range
                if (pixelY < 0 || pixelY >= orig.height())
                    continue;

                totItems++;
                pixel pixel = { ((uint)orig.pixel(pixelX, pixelY)) };
                auto maskVal = getMask(convX, convY);
                val += (pixel.r + pixel.g + pixel.b) * maskVal;
            }
        }

        // do some adjustments for edges
        double edgeAdj = ((double)totItems)/ (w * h);

        char totVal = std::min(UCHAR_MAX, ((int) (edgeAdj * val * multip / 3)));
        pixel toRet;
        toRet.a = UCHAR_MAX;
        toRet.r = toRet.g = toRet.b = totVal;

        //cout << "totVal: " << std::dec << totVal << " pixel: 0x" << std::hex << toRet << "\n";
        return toRet;
    }


    pixel getColor(const QImage& orig, int x, int y) const {
        int totItems = 0;
        int red = 0;
        int green = 0;
        int blue = 0;

        for (int convX = 0; convX < w; convX++) {
            int pixelX = x + convX - halfW;
            // skip this convolution if pixel X outside of img range
            if (pixelX < 0 || pixelX >= orig.width())
                continue;

            for (int convY = 0; convY < h; convY++) {
                int pixelY = y + convY - halfH;
                // skip this convolution if pixel Y outside of img range
                if (pixelY < 0 || pixelY >= orig.height())
                    continue;

                totItems++;
                pixel pixel = { orig.pixel(pixelX, pixelY) };
                auto maskVal = getMask(convX, convY);
                red += pixel.r * maskVal;
                green += pixel.g * maskVal;
                blue += pixel.b * maskVal;
            }
        }

        // do some adjustments for edges
        double edgeAdj = ((double)totItems)/ (w * h);
        pixel toRet;
        toRet.r = std::min(UCHAR_MAX, ((int) (edgeAdj * red * multip)));
        toRet.g = std::min(UCHAR_MAX, ((int) (edgeAdj * green * multip)));
        toRet.b = std::min(UCHAR_MAX, ((int) (edgeAdj * blue * multip)));

        return toRet;
    }


    // greyscale is whether or not convolution should be applied to each pixel color or color magnitude as a whole
    QImage apply(const QImage& orig, bool greyScale) const {
        QImage toRet = orig.copy(0,0,orig.width(),orig.height());

        auto imgWidth = orig.width();
        auto imgHeight = orig.height();

        for (int x = 0; x < imgWidth; x++) {
            for (int y = 0; y < imgHeight; y++) {
                pixel toSet = (greyScale) ?
                            getGreyScale(orig,x,y) :
                            getColor(orig,x,y);

                toRet.setPixel(x,y,toSet);
            }
        }

        return toRet;
    }
};


const short gaussBlurArr[25] = {1,4,6,4,1,
                                4,16,24,16,4,
                                6,24,36,24,36,
                                4,16,24,16,4,
                                1,4,6,4,1};

const ConvolutionMask GaussianBlur5x5(gaussBlurArr, 5, 5, ((double)1) / 256);


const short edgeDetection[9] = {-1,-1,-1,
                                -1,8,-1,
                                -1,-1,-1};

const ConvolutionMask EdgeDetection(edgeDetection, 3, 3, ((double)1) / 10);

// for testing purposes
const short identity[9] = {0,0,0,
                          0,1,0,
                          0,0,0};

const ConvolutionMask IdentityMask(identity, 3, 3, 1);



bool MainWindow::loadFile(const QString &fileName)
{
    QImageReader reader(fileName);
    reader.setAutoTransform(true);
    const QImage newImage = reader.read();
    if (newImage.isNull()) {
        QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                                 tr("Cannot load %1: %2")
                                 .arg(QDir::toNativeSeparators(fileName), reader.errorString()));
        return false;
    }

    setImage(newImage);

    setWindowFilePath(fileName);

    const QString message = tr("Opened \"%1\", %2x%3, Depth: %4")
        .arg(QDir::toNativeSeparators(fileName)).arg(image.width()).arg(image.height()).arg(image.depth());
    return true;
}

const int IMAGE_HEIGHT = 400;
const int IMAGE_WIDTH = 400;

void MainWindow::setImage(const QImage &newImage)
{
    image = newImage;
    blurred_image = GaussianBlur5x5.apply(image, false);
    edged_image = EdgeDetection.apply(blurred_image, true);

    ui->Image_Gauss->setPixmap(QPixmap::fromImage(blurred_image).scaled(IMAGE_WIDTH, IMAGE_HEIGHT, Qt::KeepAspectRatio));
    ui->Image_Edges->setPixmap(QPixmap::fromImage(edged_image).scaled(IMAGE_WIDTH, IMAGE_HEIGHT, Qt::KeepAspectRatio));
}


bool MainWindow::saveFile(const QString &fileName, const QImage& image)
{
    QImageWriter writer(fileName);

    if (!writer.write(image)) {
        QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                                 tr("Cannot write %1: %2")
                                 .arg(QDir::toNativeSeparators(fileName)), writer.errorString());
        return false;
    }
    const QString message = tr("Wrote \"%1\"").arg(QDir::toNativeSeparators(fileName));
    return true;
}

// taken primarily from qt image loading examples
static void initializeImageFileDialog(QFileDialog &dialog, QFileDialog::AcceptMode acceptMode)
{
    static bool firstDialog = true;

    if (firstDialog) {
        firstDialog = false;
        const QStringList picturesLocations = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation);
        dialog.setDirectory(picturesLocations.isEmpty() ? QDir::currentPath() : picturesLocations.last());
    }

    QStringList mimeTypeFilters;
    const QByteArrayList supportedMimeTypes = acceptMode == QFileDialog::AcceptOpen
        ? QImageReader::supportedMimeTypes() : QImageWriter::supportedMimeTypes();
    foreach (const QByteArray &mimeTypeName, supportedMimeTypes)
        mimeTypeFilters.append(mimeTypeName);
    mimeTypeFilters.sort();
    dialog.setMimeTypeFilters(mimeTypeFilters);
    dialog.selectMimeTypeFilter("image/png");
    if (acceptMode == QFileDialog::AcceptSave)
        dialog.setDefaultSuffix("png");
}

void MainWindow::saveAs(const QImage& image)
{
    QFileDialog dialog(this, tr("Save File As"));
    initializeImageFileDialog(dialog, QFileDialog::AcceptSave);
    dialog.setAcceptMode(QFileDialog::AcceptSave);

    while (dialog.exec() == QDialog::Accepted && !saveFile(dialog.selectedFiles().first(), image)) {}
}



void MainWindow::onOpen()
{
    QFileDialog dialog(this, tr("Open File"));
    initializeImageFileDialog(dialog, QFileDialog::AcceptOpen);

    while (dialog.exec() == QDialog::Accepted && !loadFile(dialog.selectedFiles().first())) {}

    ui->actionSave_Blurred->setEnabled(!image.isNull());
    ui->actionSave_Edges->setEnabled(!image.isNull());
    ui->Save_Edges->setEnabled(!image.isNull());
    ui->Save_Gauss->setEnabled(!image.isNull());
}






void MainWindow::onSaveBlurred()
{
    saveAs(this->blurred_image);
}


void MainWindow::onSaveEdges()
{
    saveAs(this->edged_image);
}

void MainWindow::on_Save_Edges_clicked()
{
    onSaveEdges();
}

void MainWindow::on_Save_Gauss_clicked()
{
    onSaveBlurred();
}

void MainWindow::on_actionOpen_triggered()
{
    onOpen();
}

void MainWindow::on_actionSave_Blurred_triggered()
{
    onSaveBlurred();
}

void MainWindow::on_actionSave_Energy_triggered()
{
    onSaveEdges();
}
