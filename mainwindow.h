#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onOpen();
    void onSaveBlurred();
    void onSaveEdges();

    void saveAs(const QImage& image);
    bool saveFile(const QString &fileName, const QImage& image);
    void setImage(const QImage& newImage);
    bool loadFile(const QString &fileName);

    void on_Save_Edges_clicked();
    void on_Save_Gauss_clicked();
    void on_actionOpen_triggered();
    void on_actionSave_Blurred_triggered();
    void on_actionSave_Energy_triggered();



private:
    Ui::MainWindow *ui;
    QImage image;
    QImage blurred_image;
    QImage edged_image;

};

#endif // MAINWINDOW_H
